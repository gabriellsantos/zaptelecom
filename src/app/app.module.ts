import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Clipboard } from '@ionic-native/clipboard';
import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { File } from '@ionic-native/file';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { OneSignal } from '@ionic-native/onesignal';


import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { InvoicePage } from '../pages/invoice/invoice';
import {SelectinvoicePage} from '../pages/selectinvoice/selectinvoice';
import {ModalEmailPage} from '../pages/modal-email/modal-email';
import {ChatbotPage} from '../pages/chatbot/chatbot';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {MesAnoPipe} from  './mesano';
import {FormularioPage} from '../pages/formulario/formulario';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    InvoicePage,
    SelectinvoicePage,
    ModalEmailPage,
    ChatbotPage,
    MesAnoPipe,
    FormularioPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    InvoicePage,
    SelectinvoicePage,
    ModalEmailPage,
    ChatbotPage,
    FormularioPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Clipboard,
    Transfer,
    File,
    InAppBrowser,
    OneSignal,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
