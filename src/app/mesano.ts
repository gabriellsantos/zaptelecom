/**
 * Created by gabrielsantos on 05/04/17.
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mesano'
})
export class MesAnoPipe implements PipeTransform {
  transform(value: string): string {
    let splited = value.split('-');
    let mes = splited[1];
    let ano = splited[0];
    let retorno = '';
    switch(mes)
    {
      case '01':
        retorno = 'Janeiro/'+ano;
         break;
      case '02':
        retorno = 'Fevereiro/'+ano;
        break;
      case '03':
        retorno = 'Março/'+ano;
        break;
      case '04':
        retorno = 'Abril/'+ano;
        break;
      case '05':
        retorno = 'Maio/'+ano;
        break;
      case '06':
        retorno = 'Junho/'+ano;
        break;
      case '07':
        retorno = 'Julho/'+ano;
        break;
      case '08':
        retorno = 'Agosto/'+ano;
        break;
      case '09':
        retorno = 'Setembro/'+ano;
        break;
      case '10':
        retorno = 'Outubro/'+ano;
        break;
      case '11':
        retorno = 'Novembro/'+ano;
        break;
      case '12':
        retorno = 'Dezembro/'+ano;
        break;
    }
    return retorno;
  }
}
