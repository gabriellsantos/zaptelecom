import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';



import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { InvoicePage } from '../pages/invoice/invoice';
import {SelectinvoicePage} from '../pages/selectinvoice/selectinvoice';
import {ModalEmailPage} from '../pages/modal-email/modal-email';
import {ChatbotPage} from '../pages/chatbot/chatbot';
import {FormularioPage} from '../pages/formulario/formulario';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
