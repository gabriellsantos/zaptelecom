import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController,ToastController, ModalController } from 'ionic-angular';
import {Http, Headers,URLSearchParams} from '@angular/http';


/*
  Generated class for the ModalEmail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-modal-email',
  templateUrl: 'modal-email.html'
})
export class ModalEmailPage {
  customer: any;
  session: any;
  fatura: any;
  public email: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public http: Http,
              public loadingCtrl: LoadingController, public toastCtrl: ToastController, public modal: ModalController) {
    this.customer = navParams.get('customer');
    this.fatura = navParams.get('fatura');
    this.email = this.customer.email;
    this.session = navParams.get('sessao');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalEmailPage');
  }

  closeModal(){
    this.viewCtrl.dismiss();
  }


  sendMail()
  {

    let headers = new Headers();
    headers.append('Content-type','application/json');
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Content-type','application/x-www-form-urlencoded');
    let body = {idUsuario:this.session['id_usuario'],
      idCliente:this.customer['id'],
      idFatura:this.fatura['id'],
      emailPara:this.email,
      emailAssunto:"Boleto solicitado - ZapTelecom",
      emailMensagem:"Segue boleto solicitado através do app.",
      sessao:this.session['sessao'],
      identificador:'app'};

    let loading = this.loadingCtrl.create({content: 'Enviando e-mail...'});
    loading.present();

    this.http.post("https://www.zaptelecom.com.br:9910/EnviarDocReceberEmail", JSON.stringify(body),{headers: headers}).map(res => res.json()).subscribe(data => {
      let retorno = data.status;
      loading.dismiss();
      if(retorno == true)
      {
        this.toast("E-mail enviado com sucesso!");
        this.closeModal();
      }
    },(err) => {
      //this.get_from_heroku(loading);
    });
  }

  get_from_heroku()
  {
    let params: URLSearchParams = new URLSearchParams();
    params.set('idUsuario', this.session.id_usuario);
    params.set('idCliente', this.customer.id);
    params.set('idFatura', this.fatura.id);
    params.set('emailPara', this.email);
    params.set('sessao', this.session['sessao']);

    let loading = this.loadingCtrl.create({content: 'Enviando e-mail...'});
    loading.present();


    this.http.get("https://zapservice.herokuapp.com/customer/send_invoice_mail",{search: params}).map(res => res.json()).subscribe(data => {
      let retorno = data.status;


      loading.dismiss();
      if(retorno == true)
      {
        this.toast("E-mail enviado com sucesso!");

      }
    },(err) => {
      loading.dismiss();
      this.toast("Não foi possível enviar o e-mail, tente novamente mais tarde!");
    });
    this.closeModal();
  }

  toast(msg)
  {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

}
