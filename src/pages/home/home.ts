import { Component } from '@angular/core';

import { NavController,NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import {App} from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  customer: any;
  constructor(public navCtrl: NavController,public navParams: NavParams,private app:App)
  {
    this.customer = navParams.get('pCustomer');

  }

  logout()
  {
    window.localStorage.setItem("password",'');
    this.app.getRootNav().setRoot(LoginPage);
    //this.navCtrl.setRoot(LoginPage);
  }

}
