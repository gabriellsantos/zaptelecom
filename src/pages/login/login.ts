import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController,ModalController,Platform } from 'ionic-angular';
import {Http,Headers} from '@angular/http';
import { OneSignal } from '@ionic-native/onesignal';
import 'rxjs/add/operator/map';

import { TabsPage } from '../tabs/tabs';
import { FormularioPage } from '../formulario/formulario';



/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

	public user: string ;//= '90950038172';
	public password: string;// = '909500';

  public tabs: any;

	private url: string = "https://zapservice.herokuapp.com/customer/auth?";
  chat: string = "https://zapservice.herokuapp.com/customer/get_chat_url";
  public url_chat: string;

	private testeJson = {"session":{"resultado":true,"codigo":"0","msg":"Login realizado com sucesso","sessao":"37f3017dbcb92b76def0832d4ebb41c6","id_usuario":49,"nome_usuario":"APLICATIVO"},"customer":{"id":3150,"nome":"AURELIANO GUSTAVO#","fantasia":"AURELIANO GUSTAVO DE QUEIROZ ARANTES","pessoa":"fisica","cpfcnpj":"909.500.381-72","sexo":"M","endereco":"308 SUL AV. LO-7 LT 1.2HM","numero":"202","complemento":"GREEN PARK","bairro":"PLANO DIRETOR SUL","cep":"77021-054","id_cidade":24,"cidade":"PALMAS","uf":"TO","id_status":1,"status":"liberado","email":"aurelianoarantes@zaptelecom.com.br","telefone":"","celular":"(63)81121-919","representante":"","representantecargo":"","representantecpf":"","representanterg":"","servicos_bases":"ERB_PMW_PANAMEIRA,ERB_PMW-ZAP,ERB REAL PARK NORTE","senhacentral":"909500"}};


  public session: any;
	public customer: any;
	public service: any;

	public appId: string = "8bd0c893-b84c-46b1-adf9-88e8bd890157";//"AAAAD3oGMJ4:APA91bHQLW5ccXGl_at0IY0vrvu_3NXuQIruHdxuOBw0D40f_oK_uKD3A7oYm8OR4gXXW0xpnOrwyrq1qHOaSZK-CMVgAYF-5c9x8si4_wvHaz0vlM3mKuFslccgHJxf3I71DoukIgCg";
	public googleProjectId: string = "297356848875";


  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http,
              public loadingCtrl: LoadingController, public toastCtrl: ToastController,
              public modal: ModalController,private _OneSignal: OneSignal, private _platform: Platform)
  {
    this.user = window.localStorage.getItem("username");
    this.password = window.localStorage.getItem("password");



    if (this.user != '' && this.password != '')
    {
      this.login();
    }
  }

  initializeApp()
  {
    this._platform.ready().then(() =>
    {
     if (this._platform.is('android') || this._platform.is('ios'))
      {
        this._OneSignal.startInit(this.appId, this.googleProjectId);
        this._OneSignal.inFocusDisplaying(this._OneSignal.OSInFocusDisplayOption.Notification);
        this._OneSignal.setSubscription(true);
        this._OneSignal.handleNotificationReceived().subscribe(() => {
          // handle received here how you wish.
        });
        this._OneSignal.handleNotificationOpened().subscribe(() => {
          // handle opened here how you wish.
        });


        this._OneSignal.endInit();

        this._OneSignal.sendTags({vencimento: this.service.vencimento, cpfcnpj: this.customer.cpfcnpj});
      }
    })
  }

  ionViewDidLoad() {

  }

  getUrl()
  {

    this.http.get(this.chat).map(res => res.json()).subscribe(data => {
      this.url_chat = data.url;
    },(err) => {

    });
  }

  login()
  {
    this.getUrl();//busca url do chat_bot

  	let loading = this.loadingCtrl.create({content: 'Aguarde...'});
  	loading.present();

  	this.http.get(this.url+"cpf="+this.user+"&password="+this.password).map(res => res.json()).subscribe(data => {


   		if(Object.keys(data).length > 0)
      {
        this.session = data.session;
        this.customer = data.customer;
        this.service = data.service;
        console.log("vencimento - "+this.service.vencimento);

        this.initializeApp();


        this.navCtrl.setRoot(TabsPage,{pSession: this.session, pCustomer: this.customer,url_chat: this.url_chat});
        loading.dismiss();

        window.localStorage.setItem("username",this.user);
        window.localStorage.setItem("password",this.password);
      }
      else
      {
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: 'Usuário ou Senha inválidos...',
          duration: 3000
        });
        toast.present();
      }
   	},(err) => {
      loading.dismiss();
      let toast = this.toastCtrl.create({
        message: 'Não foi possível se conectar ao servidor!',
        duration: 3000
      });
      toast.present();
      //this.navCtrl.setRoot(TabsPage,{pSession: this.session, pCustomer: this.customer});
    });



  }

  formulario()
  {
    this.navCtrl.push(FormularioPage,{sessao: this.session});
  }



}
