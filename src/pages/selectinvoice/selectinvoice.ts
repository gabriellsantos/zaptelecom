import { Component } from '@angular/core';
import { NavController, NavParams, ViewController,ModalController,LoadingController,ToastController } from 'ionic-angular';
import {ModalEmailPage} from "../modal-email/modal-email";
import {Http, Headers} from '@angular/http';
import { Clipboard } from '@ionic-native/clipboard';
import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { File } from '@ionic-native/file';
import { InAppBrowser } from '@ionic-native/in-app-browser';




/*
  Generated class for the Selectinvoice page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-selectinvoice',
  templateUrl: 'selectinvoice.html',
  providers: [Transfer, TransferObject, File]
})
export class SelectinvoicePage {
  item: any;
  customer: any;
  session: any;
  dados: any;
  loading: any;
  constructor(private transfer: Transfer, private file: File,
              private clipboard: Clipboard, public navCtrl: NavController,
              public navParams: NavParams, public modal: ModalController,
              public http: Http,public loadingCtrl: LoadingController,
              public toastCtrl: ToastController,private iab: InAppBrowser) {
    this.dados = "";
    this.item = navParams.get('item');
    this.customer = navParams.get('customer');
    this.session = navParams.get('sessao');
    if(this.item.pago == 'NÃO')
    {
      this.dados_fatura();
    }
    //this.dados_fatura();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectinvoicePage');
  }

  copiar(id)
  {
    //window.prompt("Copy to clipboard: Ctrl+C, Enter", document.getElementById(id).innerHTML);
    console.log("TEXTO - "+document.getElementById(id).innerHTML);
    var texto = document.getElementById(id).innerHTML;
    this.clipboard.copy(texto);
    this.toast("Copiado...");


  }

  base64()
  {

    const url = "https://zapservice.herokuapp.com/customer/base64_fatura?idUsuario="+this.session.id_usuario+"&sessao="+this.session.sessao+"&idCliente="+this.customer.id+"&idFatura="+this.item.id;
    //const browser = this.iab.create(url, '_blank', 'location=no');
    window.open(url, '_system');
  }

  opemModalEmail()
  {
    let m = this.modal.create(ModalEmailPage,{customer: this.customer, sessao: this.session, fatura: this.item});
    m.present();
  }

  dados_fatura(){
    this.loading = this.loadingCtrl.create({content: 'Buscando Dados...'});
    this.loading.present();

    this.http.get("https://zapservice.herokuapp.com/customer/dados_fatura?idUsuario="+this.session.id_usuario+"&sessao="+this.session.sessao+"&idCliente="+this.customer.id+"&idFatura="+this.item.id).map(res => res.json()).subscribe(data => {
      this.dados = data.fatura;
      console.log("DADOS - "+JSON.stringify(this.dados));

      this.loading.dismiss();
      if(this.dados == '')
      {
        this.toast("Não há dados para esta fatura!");
      }
    },(err) => {
      this.loading.dismiss();
      this.toast("Não foi possível obter os Dados!");
    });
  }

  toast(msg)
  {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

}
