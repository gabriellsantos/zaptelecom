import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController,ToastController, ModalController } from 'ionic-angular';
import {Http, Headers,URLSearchParams} from '@angular/http';

/*
  Generated class for the Formulario page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-formulario',
  templateUrl: 'formulario.html'
})
export class FormularioPage {

  public name: string;
  public phone: string;
  public address: string;
  public email: string;
  public cep: string;
  public cidade: any;
  public cidades: string;
  session: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public http: Http,
              public loadingCtrl: LoadingController, public toastCtrl: ToastController, public modal: ModalController) {
    this.session = navParams.get('sessao');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormularioPage');
    this.get_cidades();
  }

  closeModal(){
    this.viewCtrl.dismiss();
  }

  isValidForm()
  {
    if(this.name != '' && this.phone != '' && this.cidade != '')
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  send_viabilidade()
  {
    let headers = new Headers();
    headers.append('Content-type','application/json');
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Content-type','application/x-www-form-urlencoded');
    let params: URLSearchParams = new URLSearchParams();
    params.set('nome', this.name);
    params.set('telefone', this.phone);
    params.set('endereco', this.address);
    params.set('email', this.email);
    params.set('cep', this.cep);
    params.set('id_cidade', this.cidade);


    let loading = this.loadingCtrl.create({content: 'Enviando dados...'});
    loading.present();

  //{search: params, headers: headers}
    this.http.get("https://zapservice.herokuapp.com/customer/viabilidade?nome="+this.name+"&telefone="+this.phone+"&endereco="+this.address+"&email="+this.email+"&cep="+this.cep+"&id_cidade="+this.cidade).map(res => res.json()).subscribe(data => {
      let retorno = data.status;

      console.log("viabilidade - "+JSON.stringify(data));
      loading.dismiss();
      this.closeModal();
      if(retorno == true)
      {
        this.toast("Dados enviados com sucesso!");

      }
      else {
        this.toast("Não foi possível enviar os dados, tente novamente mais tarde!");
      }
    },(err) => {
      loading.dismiss();
      this.toast("Não foi possível enviar os dados, tente novamente mais tarde!");
    });
  }

  send_mail()
  {

      let params: URLSearchParams = new URLSearchParams();
      params.set('cliente', this.name);
      params.set('telefone', this.phone);
      params.set('endereco', this.address);


      let loading = this.loadingCtrl.create({content: 'Enviando e-mail...'});
      loading.present();


      this.http.get("https://zapservice.herokuapp.com/customer/send_informations",{search: params}).map(res => res.json()).subscribe(data => {
        let retorno = data.retorno;


        loading.dismiss();
        this.navCtrl.pop();
        if(retorno == 'sucess')
        {
          this.toast("E-mail enviado com sucesso!");

        }
        else {
          this.toast("Não foi possível enviar o e-mail, tente novamente mais tarde!");
        }
      },(err) => {
        loading.dismiss();
        this.toast("Não foi possível enviar o e-mail, tente novamente mais tarde!");
      });

  }

  get_cidades()
  {
    console.log("getcidades");
    let loading = this.loadingCtrl.create({content: 'Carregando Cidades...'});
    loading.present();


    this.http.get("https://zapservice.herokuapp.com/customer/cidades").map(res => res.json()).subscribe(data => {
      let retorno = data.cidades;
      this.cidades = retorno;


      loading.dismiss();
      if(Object.keys(data).length == 0)
      {
        this.toast("Não foi possível obter a lista de Cidades, tente novamente mais tarde!");

      }

    },(err) => {
      loading.dismiss();
      this.toast("Não foi possível obter a lista de Cidades, tente novamente mais tarde!");
    });

  }

  toast(msg)
  {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

}
