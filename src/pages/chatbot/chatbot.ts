import { Component } from '@angular/core';
import { NavController, NavParams,LoadingController } from 'ionic-angular';
import {Http, Headers} from '@angular/http';
import {DomSanitizer} from "@angular/platform-browser";

/*
  Generated class for the Chatbot page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-chatbot',
  templateUrl: 'chatbot.html'
})
export class ChatbotPage {

  url: string = "https://zapservice.herokuapp.com/customer/get_chat_url";
  loading: any;
  public url_chat: any;
  constructor(private domSanitizer : DomSanitizer,public navCtrl: NavController, public navParams: NavParams, public http: Http,public loadingCtrl: LoadingController,)
  {
    //this.getUrl();
    this.url_chat = this.domSanitizer.bypassSecurityTrustResourceUrl(navParams.get('url_chat'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatbotPage');
  }

  plans_count()
  {


  }

  getUrl()
  {
    this.loading = this.loadingCtrl.create({content: 'Carregando...'});
    this.loading.present();

    this.http.get(this.url).map(res => res.json()).subscribe(data => {
      this.url_chat = data.url;

      this.loading.dismiss();

    },(err) => {
      this.loading.dismiss();
    });
  }

}
