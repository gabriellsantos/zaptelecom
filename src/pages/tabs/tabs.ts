import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { HomePage } from '../home/home';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { InvoicePage } from '../invoice/invoice';
import {ChatbotPage} from '../chatbot/chatbot';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = HomePage;
  tab2Root: any = InvoicePage;
  tab3Root: any = ChatbotPage;

  public session: Array<string>;
  public customer: any;
  params: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.params = navParams.data;

  }
}
