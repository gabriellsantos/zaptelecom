import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController,ToastController, ModalController } from 'ionic-angular';
import {Http, Headers} from '@angular/http';

import 'rxjs/add/operator/map';
import {MesAnoPipe} from '../../app/mesano';

import { SelectinvoicePage } from '../selectinvoice/selectinvoice';
import {ModalEmailPage} from "../modal-email/modal-email";
import { LoginPage } from '../login/login';
import {App} from 'ionic-angular';
/*
  Generated class for the Invoice page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-invoice',
  templateUrl: 'invoice.html'
})
export class InvoicePage {

  temp: any = {
  "resultado":true,
  "dados":[ {
    "id":1,
    "datacadastro":"2017-01-10",
    "valor":"111.00",
    "vencimento":"2017-02-10",
    "pago":"SIM",
    "datapagto":"2017-01-15",
    "valorpago":"111.0",
    "pagoem":"",
    "historico":"TESTE – 01/2017",
    "numero_fatura":"",
    "periodo_inicio":"2017-01-01",
    "periodo_fim":"2017-01-31",
    "cod_portador":1,
    "portador":"BANCO DO BRASIL",
    "cod_documento":1,
    "documento":"DUPLICATA",
    "cod_plano_contas":1,
    "plano_contas":"MENSALIDADES",
    "codigo_cliente":1,
    "codigo_servico":1,
    "nome":"TOPSAPP API",
    "endereco":"Rua das cerejeiras",
    "numero":"1987",
    "bairro":"Jardim Paraíso",
    "cep":"78556-696",
    "cidade":"SINOP",
    "uf":"MT",
    "celular":"(66)9685-2854",
    "telefone":"(66)3531-5835",
    "fatura_instrucoes":"",
    "fatura_observacoes":"",
    "desmembrar_scm_sva":"sim"
  },{
    "id":1,
    "datacadastro":"2017-01-10",
    "valor":"111.00",
    "vencimento":"2017-01-10",
    "pago":"NÃO",
    "datapagto":"",
    "valorpago":"",
    "pagoem":"",
    "historico":"TESTE – 01/2017",
    "numero_fatura":"",
    "periodo_inicio":"2017-01-01",
    "periodo_fim":"2017-01-31",
    "cod_portador":1,
    "portador":"BANCO DO BRASIL",
    "cod_documento":1,
    "documento":"DUPLICATA",
    "cod_plano_contas":1,
    "plano_contas":"MENSALIDADES",
    "codigo_cliente":1,
    "codigo_servico":1,
    "nome":"TOPSAPP API",
    "endereco":"Rua das cerejeiras",
    "numero":"1987",
    "bairro":"Jardim Paraíso",
    "cep":"78556-696",
    "cidade":"SINOP",
    "uf":"MT",
    "celular":"(66)9685-2854",
    "telefone":"(66)3531-5835",
    "fatura_instrucoes":"",
    "fatura_observacoes":"",
    "desmembrar_scm_sva":"sim"
  },{
    "id":1,
    "datacadastro":"2017-01-10",
    "valor":"111.00",
    "vencimento":"2017-03-10",
    "pago":"NÃO",
    "datapagto":"",
    "valorpago":"",
    "pagoem":"",
    "historico":"TESTE – 01/2017",
    "numero_fatura":"",
    "periodo_inicio":"2017-01-01",
    "periodo_fim":"2017-01-31",
    "cod_portador":1,
    "portador":"BANCO DO BRASIL",
    "cod_documento":1,
    "documento":"DUPLICATA",
    "cod_plano_contas":1,
    "plano_contas":"MENSALIDADES",
    "codigo_cliente":1,
    "codigo_servico":1,
    "nome":"TOPSAPP API",
    "endereco":"Rua das cerejeiras",
    "numero":"1987",
    "bairro":"Jardim Paraíso",
    "cep":"78556-696",
    "cidade":"SINOP",
    "uf":"MT",
    "celular":"(66)9685-2854",
    "telefone":"(66)3531-5835",
    "fatura_instrucoes":"",
    "fatura_observacoes":"",
    "desmembrar_scm_sva":"sim"
  } ]};

  session: any;
  public customer: any;
  public invoices: any;
  loading: any;
  public tipo: string = 'pendentes';
  private url: string = 'https://www.zaptelecom.com.br:9910/ObterDocReceber';

  constructor(public navCtrl: NavController,public navParams: NavParams, public http: Http,
              public loadingCtrl: LoadingController, public toastCtrl: ToastController,
              public modal: ModalController,private app:App)
  {

    this.session = navParams.get('pSession');
  	this.customer = navParams.get('pCustomer');

  	this.getInvoices2();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvoicePage');
  }

  getInvoices2()
  {

    let headers = new Headers();
    headers.append('Content-type','application/json');
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Content-type','application/x-www-form-urlencoded');
    let body = {idUsuario:this.session['id_usuario'],
      idCliente:this.customer['id'],
      sessao:this.session['sessao'],
      identificador:'app'};

    this.loading = this.loadingCtrl.create({content: 'Buscando Faturas...'});
    this.loading.present();

    this.http.get("https://zapservice.herokuapp.com/customer/get_docs?usuario="+this.session.id_usuario+"&sessao="+this.session.sessao+"&cliente="+this.customer.id).map(res => res.json()).subscribe(data => {
      this.invoices = data.dados;

      this.loading.dismiss();
      if(this.invoices == '')
      {
        this.toast("Não ha Faturas geradas!");
      }
    },(err) => {
      this.loading.dismiss();
      this.toast("Não foi possível obter as Faturas!");
    });
  }

  getInvoices()
  {

    let headers = new Headers();
    headers.append('Content-type','application/json');
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Content-type','application/x-www-form-urlencoded');
    let body = {idUsuario:this.session['id_usuario'],
      idCliente:this.customer['id'],
      sessao:this.session['sessao'],
      identificador:'app'};

    this.loading = this.loadingCtrl.create({content: 'Buscando Faturas...'});
    this.loading.present();

    this.http.post(this.url, JSON.stringify(body),{headers: headers}).map(res => res.json()).subscribe(data => {
      this.invoices = data.dados;
      this.loading.dismiss();
    },(err) => {
      //this.get_from_heroku(loading);
      this.http.get("https://zapservice.herokuapp.com/customer/get_docs?usuario="+this.session.id_usuario+"&sessao="+this.session.sessao+"&cliente="+this.customer.id).map(res => res.json()).subscribe(data => {
        this.invoices = data.dados;


        this.loading.dismiss();
        if(this.invoices == '')
        {
          this.toast("Não ha Faturas geradas!");
        }
      },(err) => {
        this.loading.dismiss();
        this.toast("Não foi possível obter as Faturas!");
      });
    });
  }

  get_from_heroku(loading)
  {
    this.http.get("https://zapservice.herokuapp.com/customer/get_docs?usuario="+this.session.id_usuario+"&sessao="+this.session.sessao+"&cliente="+this.customer.id).map(res => res.json()).subscribe(data => {
      this.invoices = data.dados;


      loading.dismiss();
      if(this.invoices == '')
      {
        this.toast("Não ha Faturas geradas!");
      }
    },(err) => {
      loading.dismiss();
      this.toast("Não foi possível obter as Faturas!");
    });
  }

  toast(msg)
  {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  selectInvoice(item)
  {
    this.navCtrl.push(SelectinvoicePage, {item: item, customer: this.customer, sessao: this.session});
  }


  logout()
  {
    window.localStorage.setItem("password",'');
    this.app.getRootNav().setRoot(LoginPage);
    //this.navCtrl.setRoot(LoginPage);
  }


}


